export default {
	apiGateway: {
		REGION: 'YOUR_API_GATEWAY_REGION',
		URL: 'YOUR_API_GATEWAY_URL'
	},
	cognito: {
		REGION: 'YOUR_COGNITO_REGION',
		USER_POOL_ID: 'YOUR_COGNITO_USER_POOL_ID',
		APP_CLIENT_ID: 'YOUR_COGNITO_APP_CLIENT_ID',
		IDENTITY_POOL_ID: 'YOUR_IDENTITY_POOL_ID'
	}
};
//
// export default {
// 	apiGateway: {
// 		REGION: 'us-east-1',
// 		URL: ' https://g57lizquj5.execute-api.us-east-1.amazonaws.com/dev/hello'
// 	},
// 	cognito: {
// 		REGION: 'us-east-1',
// 		USER_POOL_ID: 'us-east-1_2I8zrfLLQ',
// 		APP_CLIENT_ID: '3tggcqfe5orpis02gpabfb1ec8',
// 		IDENTITY_POOL_ID: 'us-east-1:0079b155-6fff-42f6-9aad-a739757d3f1e'
// 	}
// };
